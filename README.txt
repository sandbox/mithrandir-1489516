
Nodequeue context README

CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation
  * Configuration
  * Usage


INTRODUCTION
------------
This module allows users to add contexts from nodequeues to e.g. panels if they
have the "manipulate queues" permission from the Nodequeue module.

Maintainer: Jan Keller Catalan (http://drupal.org/user/15431)
Project page: http://drupal.org/sandbox/Mithrandir/1489516.

INSTALLATION
------------
1. Copy nodequeue_context folder to modules directory.
2. At admin/build/modules enable the Nodequeue context module in the Other.


CONFIGURATION
-------------
None

USAGE
-----
Use like any other context
