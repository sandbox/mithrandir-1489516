<?php

/**
 * @file
 * Plugin to provide a node context. A node context is a node wrapped in a
 * context object that can be utilized by anything that accepts contexts.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Nodequeue"),
  'description' => t('A nodequeue object.'),
  'context' => 'nodequeue_context_context_create_nodequeue',
  'edit form' => 'nodequeue_context_context_nodequeue_settings_form',
  'defaults' => array('qid' => ''),
  'keyword' => 'nodequeue',
  'context name' => 'nodequeue',
  'convert list' => array(
    'qid' => t('Nodequeue ID'),
    'name' => t('Nodequeue name'),
    'name_dashed' => t('Nodequeue name, lowercased and spaces converted to dashes'),
  ),
  'convert' => 'nodequeue_context_context_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the ID of a nodequeue for this context.'),
  ),
);

/**
 * Create the nodequeue context.
 *
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function nodequeue_context_context_create_nodequeue($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('nodequeue');
  $context->plugin = 'nodequeue';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    $qid = is_array($data) && isset($data['qid']) ? $data['qid'] : (is_object($data) ? $data->qid : 0);

    if ($qid > 0) {
      $data = nodequeue_load($qid);
    }
  }

  if (!empty($data)) {
    $context->data     = $data;
    $context->title    = $data->title;
    $context->argument = $data->qid;

    return $context;
  }
}

/**
 * Settings configuration form for nodequeue context.
 *
 * @param array $form
 *   Form array definition
 * @param array $form_state
 *   Form state array
 */
function nodequeue_context_context_nodequeue_settings_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  $form['nodequeue'] = array(
    '#title' => t('Enter the title or QID of a nodequeue'),
    '#type' => 'textfield',
    '#maxlength' => 512,
    '#autocomplete_path' => 'nodequeue/queue/autocomplete',
    '#weight' => -10,
  );

  if (!empty($conf['qid'])) {
    $queue = nodequeue_load($conf['qid']);
    if ($queue) {
      $link = l(
        t(
          "'%title' [nodequeue id %qid]",
          array(
            '%title' => $queue->title,
            '%qid' => $queue->qid,
          )
        ),
        "nodequeue/$queue->qid",
        array(
          'attributes' => array(
            'target' => '_blank',
            'title' => t('Open in new window'),
          ),
          'html' => TRUE,
        )
      );
      $form['nodequeue']['#description'] = t('Currently set to !link', array('!link' => $link));
    }
  }

  $form['qid'] = array(
    '#type' => 'value',
    '#value' => $conf['qid'],
  );

  return $form;
}

/**
 * Validate a node.
 */
function nodequeue_context_context_nodequeue_settings_form_validate($form, &$form_state) {
  // Validate the autocomplete.
  if (empty($form_state['values']['qid']) && empty($form_state['values']['nodequeue'])) {
    form_error($form['nodequeue'], t('You must select a nodequeue.'));
    return;
  }

  if (empty($form_state['values']['nodequeue'])) {
    return;
  }

  $qid          = $form_state['values']['nodequeue'];
  $preg_matches = array();
  $match        = preg_match('/\[id: (\d+)\]/', $qid, $preg_matches);
  if (!$match) {
    $match = preg_match('/^id: (\d+)/', $qid, $preg_matches);
  }

  if ($match) {
    $qid = $preg_matches[1];
  }
  if (is_numeric($qid)) {
    $nodequeue = db_query('SELECT qid FROM {nodequeue_queue} WHERE qid = :qid', array(':qid' => $qid))->fetchObject();
  }
  else {
    $nodequeue = db_query('SELECT qid FROM {nodequeue_queue} WHERE LOWER(title) = LOWER(:title)', array(':title' => $qid))->fetchObject();
  }

  // Do not allow inexisting nodequeues to be selected by unprivileged users.
  if (!$nodequeue && !(user_access('manipulate nodequeue'))) {
    form_error($form['nodequeue'], t('Invalid nodequeue selected.'));
  }
  else {
    form_set_value($form['qid'], $nodequeue->qid, $form_state);
  }
}

/**
 * Nodequeue settings form submit function.
 */
function nodequeue_context_context_nodequeue_settings_form_submit($form, &$form_state) {
  // This will either be the value set previously or a value set by the
  // validator.
  $form_state['conf']['qid'] = $form_state['values']['qid'];
}

/**
 * Convert a context into a string.
 */
function nodequeue_context_context_convert($context, $type) {
  switch ($type) {
    case 'qid':
      return $context->data->qid;

    case 'name':
      return $context->data->name;

    case 'name_dashed':
      return drupal_strtolower(str_replace(' ', '-', $context->data->name));
  }
}
